provider "google" {
  region      = var.region
  project     = var.google_project
  credentials = base64decode("${var.google_credentials}")
}
