variable "description" {}

# Configuration variables
variable "cidr" {
  type    = string
  default = "10.10.10.0/24"
}

variable "region" {}

# Google-specific variables
variable "google_project" {}
variable "google_credentials" {}
