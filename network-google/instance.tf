resource "google_compute_network" "vpc_network" {
  name                    = "src-${var.workspace_id}-vpc"
  auto_create_subnetworks = false
  mtu                     = 1500
  # The default MTU size for google VPC is 1460 which might lead to incompatibility in certain cases like with a docker network with default MTU of 1500.
}

resource "google_compute_subnetwork" "subnet" {
  name          = "src-${var.workspace_id}-subnet"
  ip_cidr_range = var.cidr
  region        = var.region
  network       = google_compute_network.vpc_network.id
}

resource "google_compute_firewall" "rules" {
  name          = "src-${var.workspace_id}-secgroup-internal"
  network       = google_compute_network.vpc_network.id
  direction     = "INGRESS"
  source_ranges = ["${google_compute_subnetwork.subnet.ip_cidr_range}"]
  allow {
    protocol = "tcp"
    ports = ["1-65535"]
  }
}